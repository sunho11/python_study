'''
    모듈이라는 기능을 통해서 코드를 분리.
    모듈 : 여러 변수와 함수를 가지고 있는 집합체로, 표준모듈과 외부모듈
    -표준모듈 : 파이썬 내장 모듈
    -외부모듈 : 다른 사람이 공개한 모듈
    -모듈을 사용하기 위해서는 import 해두어야 한다
'''

# 1. math 모듈
import math
print(math.sin(1)) #0.8414709848078965

print(math.floor(2.5)) #2 내림!
print(math.ceil(2.3)) #3 올림
print(round(2.5)) #반올림 함수인데, 짝수이면 5를 내리고, 홀수는 5를 높인다. 그렇기 때문에 잘 사용하지 않는다



#  (1)from 구문
# : 매번 앞에 모듈 이름을 붙이기 귀찮은 경우 from 구문을 사용한다
# from 모듈이름 import 가져오고 싶은 변수 또는 함수
from math import sin, floor, ceil
print(sin(1)) #0.8414709848078965
print(floor(2.5)) #2
print(ceil(3.2)) #4

#만일 모든 것을 가져오고 싶으면, from 함수이름 import * 사용!

# (2)as 구문 
# : import 모듈 as 사용하고 싶은 식별자
# 즉, 별칭을 넣어줄 수 있다.

import math as m
print(m.sqrt(16)) #4.0





# 2. random 모듈
# : 랜덤한 값을 가져오는 모듈
import random

# random.random() :  0과 1 사이의 숫자
print("random : ",random.random()) #random :  0.7618051296432146

# random.uniform(min, max) : 지정 범위 사이의 float 값 리턴
print("random.uniform() : ", random.uniform(10,15)) #random.uniform() :  11.88961712571636

# random.randrange(max) : 0부터 max사이의 int 값 리턴
# random.randrange(min,max) : min부터 max사이의 int 값 리턴
print("random.randrange(max) : ",random.randrange(25)) #random.randrange(max) :  19

# random.choice(list) : 리스트 내부의 요소를 랜덤으로 출력
print("random.choice(list) : ",random.choice([1,2,3,4,5])) #random.choice(list) :  4

# random.shuffle(list) : 리스트의 요소들을 랜덤하게 섞음
shuffle_list=[1,2,3,4,5]
print("shuffle(list) : ",random.shuffle(shuffle_list))
for i in range(len(shuffle_list)):
    print(shuffle_list[i]) #1,5,3,4,2
#즉, 원본이 섞여서 나온다!

#sample(list, k=<숫자>) : 리스트의 요소 중에 k개를 뽑는다
print("sample(list,k) : ",random.sample([1,2,3,4,5],2)) #sample(list,k) :  [4, 5]





# 3.sys모듈
# : 시스템과 관련된 정보를 가지고 있는 모듈.
import sys

# 명령 매개변수
print(sys.argv)
print("-------") #현재 파일의 경로가 나온다!

#컴퓨터 환경과 관련된 정보
print("copyright : ",sys.copyright) 
#Copyright (c) 1991-1995 Stichting Mathematisch Centrum, Amsterdam.
#All Rights Reserved.
print("version : ",sys.version)
#version :  3.10.5 (v3.10.5:f377153967, Jun  6 2022, 12:36:10) [Clang 13.0.0 (clang-1300.0.29.30)]

# sys.exit()




# 4.os모듈
# 운영체제와 관련된 기능
import os

print("현재 운영체제 : " , os.name) # posix
print("현재 폴더 : ",os.getcwd()) #현재 폴더 :  /Users/sunholee/Desktop/self_study/python
print("현재 폴더 내부의 요소 : ",os.listdir())#현재 폴더 내부의 요소 :  ['chap04', 'chap03', 'chap02', 'chap05', '.DS_Store', 'info.txt', 'chap07', 'chap06', 'chap01', 'basic.txt']

#폴더 만들고 제거
os.mkdir("hello")
os.rmdir("hello")

#파일을 생성하고 파일 이름을 변경, 그리고 제거
with open("original.txt","w") as file:
    file.write("hello")
os.rename("original.txt","new.txt")
os.remove("new.txt")




# 5.datetime 모듈
# : 시간 관련 모듈
import datetime

print("현재 시간 출력하기 : ")
now = datetime.datetime.now()
print(now.year,"년")
print(now.month,"월")
print(now.day,"일")
print(now.hour,"시")
print(now.minute,"분")
print(now.second,"초")
print()


#시간 출력 방법
output_a=now.strftime("%Y.%m,%d %H:%M.%S")
outout_b="{}년 {}월 {}일 {}시 {}분 {}초".format(now.year, \
    now.month,\
    now.day,\
    now.hour,\
    now.minute,\
    now.second)
output_c=now.strftime("%Y{} %m{} %d{} %H{} %M{} %S{}").format(*"년월일시분초")

print(output_a) #2022.07,04 23:20.27
print(output_a) #2022.07,04 23:20.27
print(output_c) #2022년 07월 04일 23시 20분 27초
print()


#특정 시간 이후의 시간 구하기
after = now + datetime.timedelta(weeks=1, days=1, hours=1,minutes=1, seconds=1)
print(after.strftime("%Y{} %m{} %d{} %H{} %M{} %S{}").format(*"년월일시분초"))
#2022년 07월 13일 00시 26분 10초

#특정 시간 요소 교체하기
output = now.replace(year=(now.year+1))
print(output.strftime("%Y{} %m{} %d{} %H{} %M{} %S{}").format(*"년월일시분초"))
#2023년 07월 04일 23시 27분 13초






 # 6. time 모듈
 # : 유닉스타임(1970년 신년 기준)를 통해서 특정 시간동안 코드 진행을 정지할 때 많이 사용
import time

print("정지 5초 준비 ")
time.sleep(5)
print("5초후 시작!") # 5초 후 나타나게 된다!





# 7. urllib 모듈
# : URL을 다루는 모듈
from urllib import request

target = request.urlopen("https://google.com")
output = target.read()

print(output)











