# 1.불(boolean) 연산자
# ==,!=,<,>,<=,>=
print(10==100)#False
print(10!=100)#True

#사전 순서로 True,False 가능
print("가방"<"하마") #True
print("가방"<'af') #False (영어가 한글보다 빠르다)



# 2.논리 연산자
# -not 
print(not False) #True

# -and : 둘다 True일 때만 True
print(True and True) #True
print(False and True) #False

# -or : 둘 중 하나만 True
print(True or False) #True
print(False or False) #False




#3. 조건문
# : 참일 때 실행
# number = input("정수 입력하시오 : ")
# num = int(number)

# if(num>0):
#     print("양수")

# if num<0:
#     print("음수")

# if num==0:
#     print("0입니다")   



#간단한 시간 활용
import datetime
now =datetime.datetime.now()

print("{}년 {}월 {}일 {}시 {}분 {}초".format(
    now.year,
    now.month,
    now.day,
    now.hour,
    now.minute,
    now.second
))

if now.hour>12 :
    print("현재 오후 {}시 입니다!".format(now.hour-12)) #현재 오후 4시 입니다!

if 6<=now.month<=8:
    print("여름입니다")    #여름입니다!

if 6<=now.month or now.month <=8:
    print("여름입니다")    #여름입니다!    





