#if_else문
# -True와 False로 나누어서 확인

num=input("정수 입력 : ")
num=int(num)

if num%2==0:
    print("짝수입니다")
else :
    print("홀수입니다")  


#elif 구문
# -여러 개의 조건을 넣고 싶을 때 사용
num2=input("정수 입력2 : ")
num2=int(num2)

if num2%2==0 and num2!=0:
    print("짝수입니다")
elif num2%2==1 :
    print("홀수입니다")  
else:
    print("0입니다")    



#False로 변환되는 값
#  -None
#  -0 , 0.0
#  -빈 컨테이너(빈 문자열, 바이트열, 리스트, 튜플, 딕셔너리 등)
if 0:
    print("0은 True")
else:
    print("0은 False")       


if "":
    print("공백은 True")
else:
    print("공백은 False")         
    


# pass 키워드
# : 아직 개발중인 부분. 파이썬의 경우, 무조건 코드가 들어가 있어야하기 때문에 일단 미완성이라는 의미로 사용
if num%2==0:
    pass
else:
    pass






