# 1. 문자열 : format()
# -문자열 안의 중괄호에 순서대로 format 괄호 안의 값을 넣어주기
format_a = "{}만 원".format(5000)
print(format_a) #5000만 원

format_b="{} {} {}".format(100,200,300)
print(format_b)

format_c="{} {} {}".format(1,"둘",True)
print(format_c)

#{}수가 더 적으면, 그만큼만 들어가게 되고, format에서 제공하는 값의 수가 더 많으면 IndexError 발생



# 2. format()의 다양한 기능
#정수
output_a="{:d}".format(52)

#특정 칸에 출력 
output_b="{:5d}".format(52)
output_c="{:10d}".format(52)

#빈칸을 0으로 채우기
output_d="{:05d}".format(52)
output_e="{:010d}".format(52)
output_f="{:05d}".format(-52)

#기호 붙여서 출력
output_g="{:+d}".format(52)
output_h="{: d}".format(52)
output_i="{:-d}".format(52)
output_j="{: d}".format(-52)

#조합
output_1="{:+5d}".format(52)
output_2="{:+5d}".format(-52)
output_3="{:=+5d}".format(52) #부호 맨앞
output_4="{:=+5d}".format(-52)
output_5="{:+05d}".format(52)
output_6="{:+05d}".format(-52)

#부동소수점(실수형) 
output_7="{:f}".format(52.773) 
output_8="{:15f}".format(52.773) 
output_9="{:+15f}".format(52.773) 
output_10="{:015f}".format(52.773) 
output_11="{:15.2f}".format(52.773) #소수점 아래 자릿수 지정

#의미없는 소수점 제거
output_12=52.0
output_13="{:g}".format(output_12)


print(output_a) #52
print(output_b) #   52
print(output_c) #        52
print(output_d) #00052
print(output_e) #0000000052
print(output_f) #-0052

print(output_g)#+52
print(output_h)# 52
print(output_i)#52
print(output_j)#-52

print(output_1) #  +52
print(output_2) #  -52
print(output_3) #+  52
print(output_4) #-  52
print(output_5) #+0052
print(output_6) #-0052

print(output_7) #52.773000
print(output_8) #      52.773000
print(output_9) #     +52.773000
print(output_10) #00000052.773000
print(output_11) #          52.77

print(output_12) #52.0
print(output_13) #52



#3. 대소문자 바꾸기
# -upper() : 대문자로!
# -lower() : 소문자로!
str_a="abcd"
str_a.upper()
print(str_a) #abcd
print(str_a.upper()) #ABCD
# 중요한 것은, str_a의 문자열 자체가 변화하지 않는다!!!! 


#4. 양 옆의 공백 제거하기
# -strip(): 문자열 '양옆'의 공백 제거
# -lstrip(): 왼쪽 공백 제거
# -rstrip(): 오른쪽 공백 제거
str_b="   공백 테스트    "
print(str_b)#   공백 테스트    
print(str_b.strip())#공백 테스트
print(str_b.lstrip())#공백 테스트
print(str_b.rstrip())#   공백 테스트


#5. 문자열 구성 파악하기
# -isalnum() : 문자열이 알파벳 또는 숫자로만 구성되었는지 확인
# -isalpha() : 알파벳만 있는지
# -isidentifier() : 식별자로 사용 가능한지
# -isdecimal() : 정수형태인지
# -isdigit() : 숫자로 인식가능한지
# -isspace() : 공백으로만 구성되어 있는지
# -islower() : 전부 소문자인지
# -isupper() : 전부 대문자인지



#6. 문자열 찾기
# -find() : 왼쪽부터 찾아서 처음 등장하는 위치
# -rfind() : 오른쪽부터 찾아서 처음 등장하는 위치
# 없으면 -1을 출력
find_a="안녕안녕하세요"
print(find_a.find("안녕"))#0
print(find_a.rfind("안녕"))#2


#7.문자열 존재 여부
print("안녕" in find_a) #True



#8.문자열 자르기
a="10 20 30 40 50".split(" ")
print(a) #['10', '20', '30', '40', '50']