#변수 : 값을 저장할 때 사용하는 식별자

# 1.변수만들기/사용하기
#(1)변수 선언하는 방법
# : 이름과 내용을 선언
#(2)변수에 값을 할당하는 방법
# : 변수에 값을 넣는 것
#(3)변수를 참조
# : 변수에서 값을 꺼내는 방법

#선언과 할당 동시
pi=3.14159265

print(pi) #3.14159265
print(pi+2) #5.14159265


r=10
sur=pi*r*r
print(sur) #314.159265


#2. 복합 대입 연산자
# : 기존 기본연산자와 조합해서 사용가능
# -뒤의 것만큼 계산해줘서 다시 값으로 할당
#+=,-=,*=,/=,%=,**=
a=3
a**=2
print(a) #9


#3. 사용자입력:input()
# -명령 프롬프트에서 사용자로부터 데이터를 입력받을 때 사용(자바의 scanner 와 같은 기능)
string=input("인사말을 입력하세요>")
print(string)
# -자료형은 무조건 str으로 뽑히게 된다.


#4.문자열을 숫자로 바꾸기
string_a = input("압력> ")
int_a = int(string_a)
print(type(int_a)) #<class 'int'>

#물론 float()을 사용하여 실수형으로도 가능하다
string_b=input("입력2> ")
float_b= float(string_b)
print(type(float_b)) #<class 'float'>

#문자열로 바꾸기 싶은 경우 str()사용

#5.valueError에러
# : 형변환을 할 수 없는 것을 변환하려고 할 때 발생하는 에러

