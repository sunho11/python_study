# 객체 : 여러가지 속성을 가질 수 있는 대상

# 클래스 : 객체를 조금 더 효율적으로 생성하기 위해서 만들어진 구문
'''
    class 클래스이름 : 
        클래스 내용
'''

# 인스턴스 : 클래스를 가지고 직접 활용하기 위해 만드는 객체
# 인스턴스 이름(변수이름) = 클래스이름() [생성자 함수라고 한다]


# 생성자 : 클래스 이름과 같은 함수
# -클래스 내부에 __init__라는 함수를 만들면 객체를 생성할 때 처리할 내용을 작성가능.
# -클래스 내부의 함수는 첫번째 매개변수로 반드시 self를 입력해야 함. self는 '자기자신'을 나타내는 딕셔너리. 
# self가 가지고 있는 속성과 기능에 접근할 때 self.<식별자> 형태로 접근

class Student:
    def __init__(self, name, korean, math, english, science):
        self.name=name
        self.korean=korean
        self.math=math
        self.english=english
        self.science=science

student=[
    Student("윤인성",87,98,88,95),
    Student("연하진",92,98,96,98),
    Student("구지연",76,96,94,90),
    Student("나선주",98,92,96,92),
    Student("윤아린",95,98,98,98),
    Student("윤명월",64,88,92,92)
]

for s in student:
    print(s.name)
    print(s.korean)
    print(s.math)
    print(s.english)
    print(s.science)
    print()

# 그러면 모든 속성이 하나씩 찍히게 된다!

# 소멸자
# : 잘 사용하지는 않지만, 인스턴스가 소멸할 때 사용
# def __del__(self) : 내용




# 메소드
# : 클래스가 가지고 있는 함수.
# -생성자를 선언하는 방버보가 똑같지만, 마찬가지로 가장 처음의 매개변수로 self를 넣어야한다!
'''
    class 클래스이름:
        def 메소드이름(self, 추가적인매개변수):
            내용
'''

class Student:
    def __init__(self, name, korean, math, english, science):
        self.name=name
        self.korean=korean
        self.math=math
        self.english=english
        self.science=science

    def get_sum(self):
        return self.korean+self.math+\
            self.english+self.science

    def get_average(self):
        return self.get_sum()/4        

    def to_string(self):
        return "{}\t{}\t{}".format(self.name,self.get_sum(),self.get_average())

student=[
    Student("윤인성",87,98,88,95),
    Student("연하진",92,98,96,98),
    Student("구지연",76,96,94,90),
    Student("나선주",98,92,96,92),
    Student("윤아린",95,98,98,98),
    Student("윤명월",64,88,92,92)
]

print("이름","총점","평균",sep="\t")
for s in student:
    print(s.to_string())

'''
    이름    총점    평균
    윤인성  368     92.0
    연하진  384     96.0
    구지연  356     89.0
    나선주  378     94.5
    윤아린  389     97.25
    윤명월  336     84.0
'''























