'''
    프로그램 내부에서 무언가를 생성한다는 것은 메모리 위에 올린다는 의미입니다.
    메모리가 부족할 경우, 하드디스크를 대신해서 사용하는데 이를 스왑(swap)이라고 합니다. 하지만 이는 메모리보다 훨씬 느립니다
    만일 계속해서 메모리에 데이터를 올리게 되면 메모리가 가득 차버리고 계속해서 스왑을 하게 됩니다.
    
    이를 방지하기 위해서 '가비지컬렉터'라는 것을 사용합니다. 가비지 컬렉터는 다시는 사용할 일이 없는 데이터를 메모리에서 제거합니다
    그렇다면 더이상 사용할 일이 없는 데이터는 어떤 것일까? 여러가지가 있지만 가장 대표적인 것이 변수에 저장되어있지 않거나
    함수에서 활용할 수가 되겠습니다.

'''

class Test:
    def __init__(self, name) :
        self.name = name
        print("{} - 생성되었습니다".format(self.name))
    def __del__(self):
        print("{} - 파괴되었습니다".format(self.name))    


Test("A")
Test("B")
Test("C")
'''
A - 생성되었습니다
A - 파괴되었습니다
B - 생성되었습니다
B - 파괴되었습니다
C - 생성되었습니다
C - 파괴되었습니다

즉, 변수에 들어가있지 않기 때문에, 가비지컬렉터에 의해서 두번 사용되지 않을 것이라고 판단되어서 과감하게 지워버리게 됩니다.
'''
print()
a=Test("A")
b=Test("B")
c=Test("C")

'''
A - 생성되었습니다
B - 생성되었습니다
C - 생성되었습니다
A - 파괴되었습니다
B - 파괴되었습니다
C - 파괴되었습니다

변수에 저장된 경우, 프로그램이 있는 동안 유지되다가 프로그램 종료와 동시에 가비지 컬렉터에 의해서 지워지게 된다!
'''








