# 클래스의 부가적인 기능


# 1. isinstance()
# : 객체가 어떤 클래스로부터 만들어졌는지 확인하는 객체
# : isinstance(인스턴스, 클래스)

from signal import raise_signal


class Student:
    def __init__(self):
        pass

student=Student()

print("isinstance(student,Student) : ",isinstance(student,Student)) #isinstance(student,Student) :  True

# type(student)== Student 도 True를 만들 수 있지만, 상속 시에는 불가능하게 된다.

# isinstance() 는 리스트에 다양한 것들이 있을 때 확인할 수 있다. 
class Student:
    def study(self):
        print("공부를 합니다")

class Teacher:
    def teach(self):
        print("학생을 가르칩니다")

classroom=[Student(),Student(), Teacher(), Student(),Student()] 

for person in classroom:
    if isinstance(person,Student):
        person.study()
    elif isinstance(person, Teacher):
        person.teach()   
'''
공부를 합니다
공부를 합니다
학생을 가르칩니다
공부를 합니다
공부를 합니다
'''         


# 2.특수한 이름의 메소드
#(1)__str__()
class Student:
    def __init__(self, name, korean, math, english, science):
        self.name=name
        self.korean=korean
        self.math=math
        self.english=english
        self.science=science

    def get_sum(self):
        return self.korean+self.math+\
            self.english+self.science

    def get_average(self):
        return self.get_sum()/4  

    def __str__(self) :
        return "{}\t{}\t{}".format(self.name,self.get_sum(),self.get_average())

student=[
    Student("윤인성",87,98,88,95),
    Student("연하진",92,98,96,98),
    Student("구지연",76,96,94,90),
    Student("나선주",98,92,96,92),
    Student("윤아린",95,98,98,98),
    Student("윤명월",64,88,92,92)
]

print("이름","총점","평균",sep="\t")
for person in student:
    print(str(person))


'''
이름    총점    평균
윤인성  368     92.0
연하진  384     96.0
구지연  356     89.0
나선주  378     94.5
윤아린  389     97.25
윤명월  336     84.0
'''

# __str__()을 통해서 str을 불러서 문자ㅕㅇㄹ로 처리할 수 있다

# 특별한 이름 
'''
__eq__(): equal 같다 
__ne__(): not equal
__gt__(): 크다
__ge__(): 크거나같다
__lt__(): 작다
__le__(): 작거나 같다
'''



#예외 처리
class Student:
    def __init__(self, name, korean, math, english, science):
        self.name=name
        self.korean=korean
        self.math=math
        self.english=english
        self.science=science

    def get_sum(self):
        return self.korean+self.math+\
            self.english+self.science

    def get_average(self):
        return self.get_sum()/4  

    def __str__(self) :
        return "{}\t{}\t{}".format(self.name,self.get_sum(),self.get_average())

    def __eq__(self,value):
        if not isinstance(value, Student):
            raise TypeError("에러 발생!!")
        return True    

student_a=Student("윤인성",87,98,88,95)

# student_a==10
# TypeError: 에러 발생!!




#클래스 변수와 메소드
'''
    클래스 변수 만들기
    class 클래스이름:
        클래스변수 = 값


    클래스변수 접근
    클래스 이름.변수이름    
'''
class Student:

    count=0

    def __init__(self, name, korean, math, english, science):
        self.name=name
        self.korean=korean
        self.math=math
        self.english=english
        self.science=science

        Student.count+=1    
        print("{}번 째 학생이 생성되었습니다!".format(Student.count))

student=[
    Student("윤인성",87,98,88,95),
    Student("연하진",92,98,96,98),
    Student("구지연",76,96,94,90),
    Student("나선주",98,92,96,92),
    Student("윤아린",95,98,98,98),
    Student("윤명월",64,88,92,92)
]
print()
print("현재 생성된 총 학생수는 {}명입니다".format(Student.count))


'''
1번 째 학생이 생성되었습니다!
2번 째 학생이 생성되었습니다!
3번 째 학생이 생성되었습니다!
4번 째 학생이 생성되었습니다!
5번 째 학생이 생성되었습니다!
6번 째 학생이 생성되었습니다!

현재 생성된 총 학생수는 6명입니다
'''


# 클래스 함수
# : 클래스 자체에서 만들어지는 함수로, 보통 메소드랑은 크게 차이는 없다.
# @classmethod라는 클래스 데코레이터를 달아주면된다!
'''
    class 클래스이름:
        @classmethod
        def 클래스함수(cls,매개변수):
            pass
'''
# 호출할 때는 클래스이름.함수이름(매개변수)
class Student:
    count=0
    students=[]

    def __init__(self, name, korean, math, english, science):
        self.name=name
        self.korean=korean
        self.math=math
        self.english=english
        self.science=science
        Student.count+=1
        Student.students.append(self)
        

    @classmethod
    def print(cls):
        print("-------학생목록--------")
        print("이름","총점","평균",sep="\t")
        for student in cls.students:
            print(str(student))
        print("------- ------------ ---------")



    def get_sum(self):
        return self.korean+self.math+\
            self.english+self.science

    def get_average(self):
        return self.get_sum()/4  

    def __str__(self) :
        return "{}\t{}\t{}".format(self.name,self.get_sum(),self.get_average())

student=[
    Student("윤인성",87,98,88,95),
    Student("연하진",92,98,96,98),
    Student("구지연",76,96,94,90),
    Student("나선주",98,92,96,92),
    Student("윤아린",95,98,98,98),
    Student("윤명월",64,88,92,92)
]

Student.print()
