# 1. getter & setter
'''
getter와 setter는 객체를 효율적으로 사용하기 위해서 필요한 것입니다.
객체지향의 특징 중 하나인 캡슐화를 위해서 getter과 setter, 그리고 private variable을 사용합니다
'''
import math

class Circle:
    def __init__(self,radius):
        self.radius=radius
    def get_circumference(self):
        return 2*math.pi*self.radius
    def get_area(self):
        return self.radius*self.radius*math.pi


circle=Circle(10)
print("원의 둘레 : ",circle.get_circumference())
print("원의 넓이 : ",circle.get_area())
print("--------------")
print()
#그러나 원의 반지름은 음수일 수 없기 때문에, 이러한 오류를 막기 위한 방법이 필요합니다

#(1)프라이빗 변수
# : 외부에서 변수에 직접 접근하는 것을 막는다. 변수 앞에 __를 넣어주면 된다!
class Circle:
    def __init__(self,radius):
        self.__radius=radius
    def get_circumference(self):
        return 2*math.pi*self.__radius
    def get_area(self):
        return self.__radius*self.__radius*math.pi

circle=Circle(10)
print("원의 둘레 : ",circle.get_circumference())
print("원의 넓이 : ",circle.get_area())
#print("__radius에 접근합니다 : ",circle.__radius) #'Circle' object has no attribute '__radius'
print("--------------")
print()

#(2)getter, setter
# : 프라이빗 변수에 접근하기 위한 수단. 이는 간접적으로 접근하기 때문에, 변수에 대한 접근 권한 혹은 제약을 통해서 시스템으로 잡을 수 없는 오류를 잡을 수 있습니다
class Circle:
    def __init__(self,radius):
        self.__radius=radius
    def get_circumference(self):
        return 2*math.pi*self.__radius
    def get_area(self):
        return self.__radius*self.__radius*math.pi

    def get_radius(self):
        return self.__radius
    def set_radius(self, value):
        self.__radius=value    


circle=Circle(10)
print("원의 둘레 : ",circle.get_circumference())
print("원의 넓이 : ",circle.get_area())
print("__radius에 접근합니다 : ",circle.get_radius()) #__radius에 접근합니다 :  10

print("--------반지름을 2로 바꿉니다---------")
circle.set_radius(2)
print("원의 둘레 : ",circle.get_circumference())
print("원의 넓이 : ",circle.get_area())
print("__radius에 접근합니다 : ",circle.get_radius())
'''
원의 둘레 :  12.566370614359172
원의 넓이 :  12.566370614359172
__radius에 접근합니다 :  2
'''

#그래서 setter에 조건을 걸어서 사용할 수 있다!
class Circle:
    def __init__(self,radius):
        self.__radius=radius
    def get_circumference(self):
        return 2*math.pi*self.__radius
    def get_area(self):
        return self.__radius*self.__radius*math.pi

    def get_radius(self):
        return self.__radius
    def set_radius(self, value):
        if value<0:
            raise TypeError("길이는 무조건 0 이상!")
        self.__radius=value  


#(3)데코레이터를 사용한 게터와 세터
class Circle:
    def __init__(self,radius):
        self.__radius=radius
    def get_circumference(self):
        return 2*math.pi*self.__radius
    def get_area(self):
        return self.__radius*self.__radius*math.pi


    #getter
    @property
    def radius(self):
        return self.__radius
    #setter
    @radius.setter
    def radius(self, value):
        if value<0:
            raise TypeError("길이는 무조건 0 이상!")
        self.__radius=value 

print("--------------")
print()
print("데코레이터 테스트")
circle=Circle(10)
print("원의 반지름 : ",circle.radius)

circle.radius=2
print("바뀐 원의 반지름 : ",circle.radius)

