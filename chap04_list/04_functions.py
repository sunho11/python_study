# 1.리스트 관련
# -min() :최솟값
# -max() : 최댓값
# -sum() : 전체합
numbers=[12,43,51,23,61]
print(min(numbers)) #12
print(max(numbers)) #61
print(sum(numbers)) #190

# -reversed() : 리스트 뒤집기
list_a=[1,2,3,4,5]
reversed_list_a=reversed(list_a)

print(reversed_list_a) #<list_reverseiterator object at 0x100b93cd0> : 이터레이터 관련
print(list(reversed_list_a)) #[5, 4, 3, 2, 1]

#여기서 중요한 것은, reversed()를 사용해서 변수에 넣은 경우, 한번 쓰면 재활용이 불가능해진다!!

# -확장 슬라이싱
print(numbers[::-1]) #[61, 23, 51, 43, 12] : 원본에는 영향 없음

# -enumerate() : 현재 인덱스가 몇번째인지 확인
example_list=["A","B","C"]

print(list(enumerate(example_list))) #[(0, 'A'), (1, 'B'), (2, 'C')] : 튜플이라고 한다
for i,value in list(enumerate(example_list)):
    print("{}번 째 : {}".format(i,value))
#0번 째 : A
#1번 째 : B
#2번 째 : C


#2.딕셔너리
# -items()
example_dict={
    "키A":"값A",
    "키B":"값B",
    "키C":"값C"
}


print(example_dict.items()) #dict_items([('키A', '값A'), ('키B', '값B'), ('키C', '값C')])

for idx, value in example_dict.items():
    print("{} 키 : {} 값 ".format(idx, value))

# 3.리스트 내포
#(1)반복문 사용
array_a=[]

for i in range(0,20,2):
    array_a.append(i)

print(array_a) #[0, 2, 4, 6, 8, 10, 12, 14, 16, 18]

#(2)리스트 안에 for문 사용 : 최종 결과를 앞에 작성한다!!
array_b=[i*i for i in range(0,20,3)]

print(array_b) #[0, 9, 36, 81, 144, 225, 324]

#(3)조건 내포
array=["사과","자두","초콜릿","바나나","체리"]
output=[fruit for fruit in array if fruit!="초콜릿"]

print(output) #['사과', '자두', '바나나', '체리']



#구문 내부에 여러 줄을 사용했을 때
#"""\""" : 들여쓰기
#\가 없이 저 괄호에 넣으면 붙어서 출력
#혹은 이스케이프 문자를 사용한다
#혹은 소괄호로 붙여서 쓸수 있다
string = (
    "안녕"
    "하세요"
    "반갑습니다"
)
print(string) #안녕하세요반갑습니다

#-join() : 리스트의 값들을 연결해서 문자열로 출력. 중간에 앞의 문자열을 끼워넣는다
print("-연결-".join(["하나","둘","셋","넷","다섯"])) #하나-연결-둘-연결-셋-연결-넷-연결-다섯




#이터레이터 : 반복할 수 있는 것
# -이터레이터는 next()함수를 통해서 꺼낼 수 있다!!
nums=[1,2,3,4,5,6]
r_num=reversed(nums) 

index=0
while index<len(nums):

    print(next(r_num))

    index+=1

# 6 5 4 3 2 1

#reversed()나 enumerate()가 iterator로 주는 이유 : 메모리의 효율성! (기존의 리스트를 활용해서 작업하자는 의미)


