# 1.리스트의 정의와 특성
# : 다양한 데이터를 목록을 가지고 저장
list_a = [273,32,"문자열",True]
print(list_a[0]) #273
print(list_a[-2])#문자열

# -값을 덮어쓰기도 가능하다
list_a[0]=12
print(list_a[0])

# -이중으로도 사용 가능
print(list_a[2][2]) #열

# -list 안에 list 사용 가능
list_b=[[0,1,2],[3,4,5],[6,7,8]]
print(list_b[2][2]) #8



# 2.리스트 연산자 : +,*,len()
list_1=[1,2,3]
list_2=[4,5,6]

list_3=list_1+list_2
print(list_3) #[1, 2, 3, 4, 5, 6]
print(list_1*3) #[1, 2, 3, 1, 2, 3, 1, 2, 3]
print(len(list_2)) #3




# 3.리스트에 요소 추가하기
#(1)append
list_4=[1,2,3]
list_4.append(4)
print(list_4) #[1, 2, 3, 4]
#(2)insert : 위치, 요소
list_4.insert(1,5)
print(list_4) #[1, 5, 2, 3, 4]



# 4.인덱스로 제거하기
#(1)del
del list_4[1]
print(list_4) #[1, 2, 3, 4]
# -범위로 제거도 가능하다
list_5=[1,2,3,4,5,6,7]
del list_5[1:3]
print(list_5) #[1, 4, 5, 6, 7]

#(2)pop : 인덱스
list_4.pop(3)
print(list_4) #[1, 2, 3]

#(3)remove : 값. 이 경우는 앞에서부터 해당하는 것을 삭제하는 것이기 때문에, 여러개의 같은 값을 삭제하고 싶으면 반복문 써야한다!
list_5.remove(1)
print(list_5) #[4, 5, 6, 7]

#(4)clear 
list_5.clear()
print(list_5)





# 5.in/not in
#(1)in : 있는지 확인
#(2)not in : 없는지 확인
list_in = [1,2,3,4,5,6,7,8,9,10]

print(7 in list_in) #True
print(11 in list_in) #False
print(11 not in list_in) #True




# 6.for 반복문
for i in range(5):
    print("출력")

for e in list_in:
    print(e) #하나씩 출력


