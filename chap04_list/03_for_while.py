# 1.범위
# -한개 : range(A) => 0부터 A-1까지
# -두개 : range(A,B) => A부터 B-1까지
# -세개 : rnage(A,B,C) => A부터 B-1까지 C만큼의 간격
list_a=[1,2,3,4,5,6,7,8,9,10]
for i in range(5):
    print(list_a[i]) #1 2 3 4 5

for i in range(2,5):
    print(list_a[i]) #3 4 5

print("----")

for i in range(1,10,3):
    print(list_a[i]) #2 5 8

#반대로도 출력 가능하다
#(1)매개변수 사용
for i in range(4, -1, -1):
    print("{}번째 : {}".format(i,list_a[i]))

#(2)reversed사용
for i in reversed(range(5)):
    print("{} index".format(i))    


#while
# while boolean :
#   식
    
i=0
while i<10:
    print(i)
    i+=1

a=1
while a in list_a:
    print(a)
    list_a.remove(a)
    a+=1
print(list_a)   # 특징상 하나씩 다 빠져버린다! 



#3. break, continue
t=0
while True:
    if t%2==0 and t!=0:
        print(str(t)+" : 짝수입니다!")
    elif t%2==1:
        print(str(t)+" : 홀수입니다")    
    else:
        print("0입니다")


    t+=1

    if t==20:
        break   

#break는 멈추는 코드
#continue는 가장 가까운 반복문을 다음 것으로 돌리는 코드

