# 1.dictionary
# -키와 값으로 저장하는 것. 자바에서는 Map
# -중괄호로 정의
# -키를 문자열로 사용할 때에는 따옴표를 꼭 사용하자!
dictionary={
    "name":"Sun",
    "type":"developer",
    "study":["java","oracle","mybatis"],
    "origin":"seoul"
}

print(dictionary["name"])
print(dictionary["type"])
for element in dictionary["study"]:
    print("- ",element)
print(dictionary["origin"])    



# 2.딕셔너리 값 추가/제거
#(1)추가
# :그냥 새로운 키값으로 선언
dictionary["new"]="new"
print(dictionary["new"])
# : 기존에 있는 키값인 경우, 값을 덮어쓰기도 가능
dictionary["name"]="won"
print(dictionary["name"])

#(2)삭제
#del 키워드 사용
del dictionary["new"]
#print(dictionary["new"]) #KeyError 발생 : 없는 키에 접근할 때 발생



# 3.키가 있는지 확인
#(1)in
key = input("입력하세요> ")
if key in dictionary:
    print(dictionary[key])
else:
    print("없어요")

#(2)get
# : key에 맞는 값을 뽑아냄. 없으면 None을 출력(에러가 아닌게 차이점)
value=dictionary.get(key)
if value ==None:
    print("흐음 없군요")
else:
    print(dictionary[key])


