# 1. 재귀함수
# : 자기 자신을 다시 부르는 함수
# (1)팩토리얼 구하기

#- for문 사용
def factorial(n):
    output=1

    for i in range(1,n+1):
        output*=i

    return output


print(factorial(5))    #120
print(factorial(4))    #24


# -재귀함수 사용
def factorial_2(n):
    if n==1:
        return 1

    return n*factorial_2(n-1)

print(factorial_2(3))    

#(2)피보나치 수열
def fibonacci(n):
    if n==1 or n==2:
        return 1

    return fibonacci(n-1)+fibonacci(n-2)

print(fibonacci(6))   #8


# 그러나 이는 stackoverflow가 일어날 가능성높음 (연산을 너무 많이하기 때문)

#global 키워드
# : 범위 밖에 선언되어 있는 키워드 지칭


#(3)메모화
# : 계산한 값을 저장해서 중복된 계산을 최소화하는 방법. (자바의 dp)
dictionary={
    1: 1,
    2: 2
}

counter=0

def fibonacci_memo(n):

    global counter
    counter+=1

    if n in dictionary:
        return dictionary[n]
    else:
        output=fibonacci_memo(n-1)+fibonacci_memo(n-2)
        
        dictionary[n]=output

        return output

print("fibonacci_memo(10) : ",fibonacci_memo(10),", counter : ",counter ) #fibonacci_memo(10) :  89 , counter :  17
print(dictionary) #{1: 1, 2: 2, 3: 3, 4: 5, 5: 8, 6: 13, 7: 21, 8: 34, 9: 55, 10: 89}




#2. 코드에 이름붙이기
# 가독성 때문에 함수 많이 사용. 
# 가독성을 위해서 중간에 주석을 사용해서 간단한 설명들을 넣어두도록 하자!


#3. 코드 유지보수
# 파이(PI=3.14) 같은 경우, 숫자를 그냥 써도 된다.
# 하지만 저렇게 단어로 변수를 지정해둔다면, 코드를 작성한 사람 외에도 쉽게 코드를 읽고 유지보수할 수 있다