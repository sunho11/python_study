# 1. 튜플
# : 리스트와 비슷한 자료형이지만, 한번 결정된 요소는 바꿀수 없다!

tuple_test = (10,20,30)

for i in range(len(tuple_test)):
    print("{}번째 : {}".format(i+1,tuple_test[i])) 

#1번째 : 10
#2번째 : 20
#3번째 : 30

#(1)튜플은 하나만 요소가 들어갈 때 반드시 뒤에 ,를 넣어주어야 한다! (10,)

#(2)튜플은 괄호 없이도 사용할 수 있다.
tuple_1=10,20,30,40
print(tuple_1) #(10, 20, 30, 40)

#(3)변수에도 직접 할당할 수 있다,
#(a,b,c)=10,20,30
a,b,c = 10,20,30 #괄호가 없어도 사용이 가능하다

print("a:{} , b;{} , c:{}".format(a,b,c)) #a:10 , b;20 , c:30

#(4) 변수의 값을 교환할 수도 있다!
d,e=10,20
print("(1) d:{}, e:{}".format(d,e)) #(1) d:10, e:20
d,e=e,d
print("(2) d:{}, e:{}".format(d,e)) #(2) d:20, e:10 
#이렇게 선언을 통해서 값을 바꿔줄 수도 있다

#(5) 튜플과 함수
def test():
    return 10,20

a,b=test()

print("a:{} , b:{}".format(a,b)) #a:10 , b:20


#(6)예시
# -divmod() : 몫과 나머지를 튜플로 반환

a,b=divmod(10,3)

print("a:{} , b:{}".format(a,b)) #a:3 , b:1


##################################################



#2. 람다
# : 값이 아닌 '함수'를 매개변수로 전달하는 기능
def call_10_times(func):
    for i in range(10):
        func()

def print_hello():
    print("hello")


call_10_times(print_hello) #hello가 10번 찍히게된다


#(1) filter() & map()
# : 함수를 매개변수로 전달하는 대표적인 표준함수
# map(함수, 리스트) : 리스트의 요소를 함수에 넣고 리턴된 값으로 새로운 리스트를 구성해 주는 함수
# filter(함수, 리스트) : 리스트의 요소를 함수에 넣고 리턴된 값이 True인 것으로 새로운 리스트를 구성

def power(item):
    return item*item

def is_under_3(item):
    return item<3


list_input_a=[1,2,3,4,5]

# -map
output_a=map(power,list_input_a)
print("map(power, list_input_a) : ", output_a) #  <map object at 0x100fd7fd0>
print("map(power, list_input_a) : ", list(output_a)) #[1, 4, 9, 16, 25]

# -filter
output_b=filter(is_under_3, list_input_a)
print("filter(is_under_3 , output_b) : ",output_b) #<filter object at 0x1042ffca0>
print("filter(is_under_3 , output_b) : ",list(output_b)) # [1, 2] 

#위에 이상한 문자가 나오는 것을 generator라고 한다!

#(2) 람다의 개념
# 매개변수로 함수 전달을 용이하게 하기 위해서 개발!
# lambda 매개변수:리턴값
power = lambda x:x*x
is_under_3 = lambda x : x<3
#위처럼 선언하면 앞서의 결과와 같게 된다

#람다는 함수도 복잡하게 선언할 수 없고, 읽기 쉽지 않은데 왜 선언할까?
#람다는 함수의 매개변수에 곧바로 넣어서 깔끔하게 선언할 수 있기 때문!!
list_input_b=[1,2,3,4,5]

output_c=map(lambda x:x*x-2, list_input_b)
print(list(output_c)) #[-1, 2, 7, 14, 23]

output_d=filter(lambda x:x<3, list_input_b)
print(list(output_d)) #[1, 2]





