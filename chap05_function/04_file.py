# 파일처리
'''
    파일을 처리하는 함수로, 표준함수가 기본으로 제공
    텍스트 파일과 바이너리 파일로 나뉘는 데, 일단 텍스트 파일만 확인
'''

#(1) 파일 열기
# 파일 객체 = open(문자열: 파일 경로, 문자열: 읽기 모드)
#모드는 w(wirte 모드), a(append 모드 : 뒤에 이어서 쓰기 모드), r(read 모드)
# 파일 닫을 때는 close()

file =open("basic.txt","w")
file.write("Hello")
file.close()

#(2) with 키워드
'''
    프로그램이 길면 open() 함수와 close() 함수에 많은 코드가 들어가게 되는데,
    그 때 열고 닫지 않는 실수가 일어날 수 있다.
    이런 실수를 막기 위해서 with키워드를 사용. with 키워드가 끝날 때 자동으로 닫히게 된다!
'''
#with open(문자열:파일 경로, 문자열:모드) as 파일 객체 : 
#               문장

#(3) 텍스트 읽기
# write() 함수 사용. 읽을 때에는 read() 함수 사용
with open("basic.txt","r") as file:
    contents =file.read()
print(contents) #Hello



#(4) 텍스트 한 줄씩 읽기
'''
    텍스트를 사용ㅇ해 데이터를 구조적으로 표현하는 방법으로는 CSV, XML, JSON 등이 있다
    이 중 CSV(Comma Separated Values) 확인.
    CSV는 한 줄에 하나의 데이터를 나타내며, 각각의 줄은 쉼표를 사용해 데이터 구분
    첫번째 줄에 헤더(header)를 넣어서 각 데이터가 나타내는 의미를 설명.

    파일을 처리할 때에는 한줄을 읽어서 처리한 후, 그 다음 파일을 처리하기 때문에 한 줄씩 읽는 방법을 알아보자
'''

import random
hanguls = list("가나다라마바사")

with open("info.txt","w") as file:
    for i in range(10):
        #랜덤한 값으로 변수를 생성
        name = random.choice(hanguls)+random.choice(hanguls)
        weight=random.randrange(40,100)
        height=random.randrange(140,200)

        #텍스트 쓰기
        file.write("{}, {}, {}\n".format(name,weight,height))

'''
    info.txt 결과 : 

    사라, 49, 189
    라바, 62, 185
    라사, 90, 178
    다마, 52, 173
    바라, 89, 183
    다다, 51, 140
    사사, 77, 156
    마나, 91, 157
    나라, 85, 195
    다라, 66, 178
'''

# 읽어올 때에는 for문을 통해서 한 줄씩 읽어오게 된다!! 
# 이 info.txt에서 읽어내면서 키와 몸무게로 BMI를 만들어보자!
with open("info.txt","r") as file:
    for line in file:
        #변수 선언
        name, weight, height = line.strip().split(", ")

        #데이터가 문제없는지 확인합니다 : 문제가 있으면 지나감
        if (not name) or (not weight) or (not height):
            continue

        bmi=int(weight) /((int(height) / 100)**2)
        result=""
        if 25 <=bmi:
            result="과체중"
        elif 18.5<=bmi:
            result="정상 체중" 
        else:
            result="저체중"

        #출력
        print('\n'.join([
            "이름: {}",
            "몸무게: {}",
            "키: {}",
            "BMI: {}",
            "결과: {}"
        ]).format(name,weight,height,bmi,result))  
        print()         





# Tip. 제너레이터
'''
    이터레이터를 직접 만들 때 사용하는 코드.
    함수 내부에 yield 키워드를 사용하면 해당 함수는 제너레이터 함수가 되며, 
    일반 함수와 달리 함수를 호출해도 함수 내부의 코드가 실행되지 않는다.
'''

def test():
    print("함수 호출")
    yield "test"

print("A 지점 통과")
test()

print("B 통과")
test()
print(test())

'''
    A 지점 통과
    B 통과
    <generator object test at 0x10523e490>
'''
# test()가 호출되지 않고, generator object만 내보내게 된다.
# next()를 통해서 함수 내부의 코드 실행
def test1():
    print("---a---")
    yield 1
    print("---b---")
    yield 2
    print("---c----")

output = test1()

# next() 함수 호출
print("---d---")
a = next(output)
print(a)
print("---e---")
b = next(output)
print(b)
print("---f---")
c = next(output)
print(c)
next(output)

'''
---d---
---a---
1
---e---
---b---
2
---f---
---c----
Traceback (most recent call last):
  File "/Users/sunholee/Desktop/self_study/python/chap05/04_file.py", line 147, in <module>
    c = next(output)
StopIteration
'''

# next() 함수를 호출할 때마다 a,b,c부분이 하나씩 통과한다.(yield가 나오면, 거기서 끝. 그다음 next()에서 그 다음 시작!)
# 만일 next() 이후 yield()를 만나지 못하면 StopIteration 에러 발생!
# 즉, 제너레이터 객체는 함수의 코드를 조금씩 실행할 때 사용!! => 메모리의 효율성










