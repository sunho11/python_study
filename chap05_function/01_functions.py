#1. 함수의 기본
#def 함수 이름():
#    문장
def print_3_times():
    for i in range(3):
        print(str(i)+"번째 입니다!")

print_3_times()
#0번째 입니다!
#1번째 입니다!
#2번째 입니다!



#2. 매개변수
#def 함수 이름(매개변수, 매개변수):
#    문장
def print_n_times(value,n):
    for i in range(n):
        print(str(i+1)+"번째 "+value+" 간다!")

print_n_times("안녕하세요", 5)

#1번째 안녕하세요 간다!
#2번째 안녕하세요 간다!
#3번째 안녕하세요 간다!
#4번째 안녕하세요 간다!
#5번째 안녕하세요 간다!


#3. 가변 매개변수
# -보통은 매개변수의 개수와 호출시 변수의 개수가 같아야한다
# -가변 매개변수는 매개변수를 원하는 만큼 받을 수 있다
#def 함수 이름(매개변수, 매개변수, ... , *가변 매개변수):
#    문장

# -가변 매개변수 뒤에는 일반 매개변수가 올 수 없다
# -가변 매개변수는 하나만 사용 가능!

def print_n_times_2(n, *values):
    for i in range(n):
        for v in values:
            print(v)
        print()


print_n_times_2(3,"하나","둘","셋","넷","다섯")

#하나
#둘
#셋
#넷
#다섯  
#위의 5개가 3번 반복!


#4. 기본 매개변수
# -매개변수를 입력하지 않아도 매개변수에 들어가는 기본값
# -'매개변수=값'의 형태
# -기본 매개변수 뒤에는 일반 매개변수가 올 수 없음
def print_n_times_3(value, n=2):
    for i in range(n):
        print(value)


print_n_times_3("테스트3")




#5. 키워드 매개변수
# -기본 매개변수와 가변 매개변수를 같이 사용할수 있도록 하기 위한 기능
# -가변 매개변수를 먼저 써주고, 뒤에 기본 매개변수의 이름과 값을 =으로 써주면 된다!
def print_n_times_4(*values,n=2):
    for i in range(n):
        for v in values:
            print(v)
        print()


print_n_times_2(3,"하나","둘","셋","넷","다섯")



#6. 기본 매개변수 중에서 필요한 값만 입력하기 
def test(a, b=10,c=100):
    print(a+b+c)

#(1)기본형태
test(10,20,30) #60
#(2)키워드 매개변수로 모든 매개변수를 지정한 형태
test(a=10,b=100,c=200) #310
#(3)키워드 매개변수로 모든 매개변수를 마구잡이로 지정
test(c=10,a=100,b=200) #310
#(4)키워드 매개변수로 일부 매개변수만 지정
test(10,c=200) #220




#7. 리턴
# -함수의 결과값
#(1)자료 없이 리턴하기
def return_test_1():
    print("A 위치입니다")
    return
    print("B 위치입니다")

return_test_1() #A 위치입니다
#즉, return하면 함수가 거기서 끝난다!

#(2)자료와 함께 리턴
def return_test_2():
    return 100

print(return_test_2()) #100


#(3)아무것도 리턴하지 않았을 때
def return_test_none():
    return

print(return_test_none()) #None



