# 예상한 예외가 나타났을 때 '예외 객체'라는 곳에 담아둘 수 있다
'''
    try:
        예외 발생 가능성 구간
    except 예외의 종류 as 예외 객체를 활용할 변수이름:
        예외가 발생했을 때 실행할 구문    
'''

#예외가 발생했을 때, 종류를 모르겠으면 Exception으로 사용!
try:
    number_input_a=int(input("정수를 입력하세요 : "))

    print("원의 분지름 : ",number_input_a)
    print("원의 둘레 : ",2*3.14*number_input_a)
    print("원의 넓이 : ", 3.14*number_input_a*number_input_a)
except Exception as ex:
    print("type(ex) : ",type(ex))
    print("exception : ",ex)

'''
    type(ex) :  <class 'ValueError'>
    exception :  invalid literal for int() with base 10: 'y'
'''


# 예외 구분하기
'''
    try:
        예외발생 구문
    except 예외종류 A:
        예외 A가 발생했을 때의 구문
    except 예외종류 B:
        예외 B가 발생했을 때의 구문        
    except 예외종류 C:
        예외 C가 발생했을 때의 구문
'''

list_number=[1,2,3,4,5]

try:
    number_input=input("정수를 입력하세요 : ")
    print("{} 번째 요소 : {}".format(number_input, list_number[number_input]))
except ValueError:
    print("정수로 다시 해주세요!")    
except IndexError:
    print("리스트의 인덱스를 벗어났어요!")

# 이 경우, 숫자가 아닌 값을 넣으면 valueError로, 인덱스를 넘으면 IndexError로 간다!

# 앞서 배운 예외 객체로도 작성 가능 (as 키워드 사용)
try:
    number_input=int(input("정수를 입력하세요 : "))
    print("{} 번째 요소 : {}".format(number_input, list_number[number_input]))
except ValueError as ex:
    print("정수로 다시 해주세요!")    
    print("exception : ",ex)
except IndexError as ex: 
    print("리스트의 인덱스를 벗어났어요!")
    print("exception : ",ex)

# 7을 넣으면 exception :  list index out of range 발생!

# 정수가 아닌 값을 넣으면 exception :  invalid literal for int() with base 10: 'd'


#이렇게 예외를 종류별로 나눠놨는데, 만일 내가 선언한 예외중에 없다면 try except문이 강제로 종료되어버린다
# 그래서 보통 마지막에 Exception으로 해둔다
try:
    number_input=int(input("정수를 입력하세요 : "))
    print("{} 번째 요소 : {}".format(number_input, list_number[number_input]))
    예외.발생()
except ValueError as ex:
    print("정수로 다시 해주세요!")    
    print("exception : ",ex)
except IndexError as ex: 
    print("리스트의 인덱스를 벗어났어요!")
    print("exception : ",ex)
except Exception as ex:
    print("뭔지 모르지만 에러발생!")



#raise
# : pass와 마찬가지로 미구현된 부분에 사용. 강제로 try except문을 종료하기 위해서 사용
# raise 뒤에 예외 이름을 입력
number = input("정수 입력 : ")
number = int(number)

if number>0:
    raise NotImplementedError
else:
    raise NotImplementedError    

