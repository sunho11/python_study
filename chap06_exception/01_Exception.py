# 1.오류
'''
    오류는 크게 두가지가 있다
    프로그램 실행 전 & 프로그램 실행 중
    프로그램 실행 전 : 구문 오류. Syntex error
    프로그램 실행 중 : 예외. Exception, runtime error
'''

# - 구문 오류
# : 괄호의 개수, 들여쓰기 문제 등으로 인해 프로그램이 실행되기도 전에 발생하는 오류
#print("# 예외 발생1) =>SyntaxError

# - 예외
# : 실행 중에 발생하는 오류
# print(exception_list[5]) : NameError. 즉 선언이 되어있지 않다는 것을 읨





# 기본 예외 처리
# 1. 조건문 
number_input_a=int(input("정수를 입력하세요 : "))

print("원의 분지름 : ",number_input_a)
print("원의 둘레 : ",2*3.14*number_input_a)
print("원의 넓이 : ", 3.14*number_input_a*number_input_a)

#그런데 이 때, input에 정수가 아닌 것을 넣는다면 에러가 발생하게 된다!
# 예시 : ValueError: invalid literal for int() with base 10: '7cm'

#이러한 것들을 조건문으로 되는 경우만 확인하자!
user_input_a= input("입력하세요 : ")
if user_input_a.isdigit():
    number_input_a=int(user_input_a)

    print("원의 분지름 : ",number_input_a)
    print("원의 둘레 : ",2*3.14*number_input_a)
    print("원의 넓이 : ", 3.14*number_input_a*number_input_a)
else:
    print("정수를 입력하세요!! ")

# 2.try except 구문
'''
    try:
        예외가 발생할 가능성이 있는 코드
    except:
        예외가 발생했을 때 실행할 코드    
'''

try:
    number_input_a=int(input("정수를 입럭하세요 : "))
    print("원의 분지름 : ",number_input_a)
    print("원의 둘레 : ",2*3.14*number_input_a)
    print("원의 넓이 : ", 3.14*number_input_a*number_input_a)
except:
    print("에러발생!")

# 만일 except에 아무것도 넣지 않고 싶다면 pass 키워드를 넣으면 된다!




# 3. try except else 구문
'''
    try:
        예외가 발생할 가능성이 있는 코드
    except:
        예외가 발생했을 때 실행할 코드 
    else:
        예외가 발생하지 않았을 때 실행할 코드

    try에는 에러가 날 만한 코드만 넣어두고, 나머지를 else로 빼는 코드이다!        
'''

try:
    number_input_a=int(input("정수를 입럭하세요 : "))
    
except:
    print("에러발생!")
else:
    print("원의 분지름 : ",number_input_a)
    print("원의 둘레 : ",2*3.14*number_input_a)
    print("원의 넓이 : ", 3.14*number_input_a*number_input_a)

# 물론, try 구문에 넣어서 처리해도 된다. 그렇지만 알아두어야 코드를 이해할 수 있다!


# 4. finally
# : 예외와 상관 없이 무조건 실행할 코드
'''
    try:
        예외가 발생할 가능성이 있는 코드
    except:
        예외가 발생했을 때 실행할 코드 
    else:
        예외가 발생하지 않았을 때 실행할 코드
    finally:
        무조건 발생할 코드         
'''
# 보통 finally코드 경우는 파일을 닫을 때 사용한다. 열린 상태에서 에러가 난 경우, 무조건 finally를 통해서 닫아주어야 한다!
# 그렇지만 사실 그냥 try except 문이 끝난 후 닫아버리면 된다








